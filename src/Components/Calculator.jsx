import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card, Button, ListGroup} from 'react-bootstrap';
import './Style.css';
import Result from './Result';

class Calculator extends Component {

    constructor() {
        super();
        this.state = {
            val1 : 0,
            val2 : 0,
            sign : "+",
            resultList : []
        };
        this.calculate = this.calculate.bind(this);
    }
    calculate(){
        var v1 = this.state.val1;
        var v2 = this.state.val2;
        var result;
        var pattern = /[^0-9]/gi; 
        if(v1 != "" || v2 != ""){
            if(v1.match(pattern) !=null || v2.match(pattern) !=null){
                alert("Cannot Calculate.!");
            }else{
                if(this.state.sign === "+"){
                    result = Number(v1) + Number(v2);
                }
                else if(this.state.sign === "-"){
                    result = Number(v1) - Number(v2);
                }
                else if(this.state.sign === "*"){
                    result =Number(v1) * Number(v2);
                }
                else if(this.state.sign === "/"){
                    result = Number(v1) / Number(v2);
                }
                else if(this.state.sign  === "%"){
                    result = Number(v1) % Number(v2);
                }
                this.setState(
                    {resultList : this.state.resultList.concat(result)}
                );
            }
        }else{
            alert("Cannot Calculate.!");
        }
    }
    render() {

        var view = this.state.resultList.map((item, i) =>(
            <Result item={item} key={i}/>
        ));

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <Card>
                            <Card.Img variant="top" src="https://is2-ssl.mzstatic.com/image/thumb/Purple123/v4/d3/ba/57/d3ba57c1-8c23-fe51-4f54-7f6307424a8c/source/512x512bb.jpg" />
                            <Card.Body>
                                <input type="text" name="value1"
                                        onChange={(evt)=>{
                                            this.setState({val1 : evt.target.value});
                                        }} />
                                <input type="text" name="value2" 
                                        onChange={(evt)=>{
                                            this.setState({val2 : evt.target.value});    
                                        }}
                                />
                                <select name="sign"
                                        onChange={(evt)=>{
                                            this.setState({sign : evt.target.value});
                                        }}            
                                >
                                    <option value="+">+ Add</option>
                                    <option value="-">- Substract</option>
                                    <option value="*">* Multiply</option>
                                    <option value="/">/ Divide</option>
                                    <option value="%">% Module</option>
                                </select>
                                <Button variant="primary" onClick={this.calculate}>Calculate</Button>
                            </Card.Body>    
                            </Card>
                        </div>
                        <div className="col-md-4">
                            <h1>Result History</h1>
                            <ListGroup>{view}</ListGroup>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calculator;