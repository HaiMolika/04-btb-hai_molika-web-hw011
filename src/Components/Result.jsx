import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ListGroup} from 'react-bootstrap'; 

export default class Result extends Component {
    render() {
        return (
            <div>
              <ListGroup.Item>{this.props.item}</ListGroup.Item>  
            </div>
        )
    }
}
